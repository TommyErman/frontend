//untuk sidenav-mobile
const sideNav = document.querySelectorAll('.sidenav');
M.Sidenav.init(sideNav);



//slider

const slider = document.querySelectorAll('.slider');
M.Slider.init(slider, {
  indicators: false,
  height: 500,
  duration: 500,
  interval: 3000
});


//paralax clinet

const paralax = document.querySelectorAll('.parallax');
M.Parallax.init(paralax);


//matrial box

const material = document.querySelectorAll('.materialboxed');
M.Materialbox.init(material);


//scrollspy

const scroll = document.querySelectorAll('.scrollspy');
M.ScrollSpy.init(scroll, {
  scrollOffset: 50
});